ChalmersKurser.se
================================
Course website [finder](http://chalmerskurser.se), redesigned course websites [(example TDA416)](http://chalmerskurser.se/tda416)
 and a [service](http://chalmerskurser.se/grupprum) making it easier booking study rooms (via TimeEdit).

## Updates

#### 2014-04-22 Study room [booking service](http://chalmerskurser.se/grupprum)
- Book TimeEdit study rooms from a mobile optimized site. Still in Beta though!

#### 2014-04-07 Course website [finder](http://chalmerskurser.se)
- New design of homepage.
- Saves the two most recent found course websites.

#### 2014-02-15 Course website [finder](http://chalmerskurser.se)
- Find course websites from course code
- Search in chrome's omnibar
- Search by going to chalmerskurser.se/{course code}

#### 2014-02-15 Redesigned [(course website)](http://chalmerskurser.se/tda416) for TDA416
- Mobile optimized.
- Subscribe to news updates (from the the official course website).
- Possible to add public notes.

## Contribute

Feel free making pull requests, posting issues or ask to get write permission to the repo.

Contact me at [simonbengtsson.se](http://simonbengtsson.se)