var COURSE_ONE = "course_one";
var COURSE_TWO = "course_two";

$("document").ready(function () {
    $search = $("#search");
    $search.keyup(function (event) {
        if (event.keyCode == 13) {
            var searchTerm = $("#search").val();
            if(searchTerm === 'about') {
                $('body').children().hide();
                $('#simon').show('slow');
            } else if(searchTerm === 'alle') {
                $('body').children().hide();
                $('#alle').show('slow');
            } else {
                search(searchTerm);
                hideKeyboard($search);
            }
        }
    });

    try {
        var courseOne = JSON.parse(localStorage.getItem(COURSE_ONE));
        var courseTwo = JSON.parse(localStorage.getItem(COURSE_TWO));
    } catch (e) {
        console.error("Coudn't load courses.");
    }
    setCourses(courseOne, courseTwo);

});

var isSearching = false;
function search(code) {
    var errors = $("#errors");
    errors.hide();
    if (isSearching) {
        return;
    }
    isSearching = true;

    $("#loading").show();
    $.get("/api/courses/", {searchTerm: code}, function (res) {
        try {
            res = JSON.parse(res);
        } catch (e) {
            errors.text("API error: " + res);
            errors.show();
        }
        if (res.success) {
            var course = res.results[0];

            if (lsTest() && $("#course-one").find("h3").text() !== course.code) {
                localStorage.setItem(COURSE_TWO, localStorage.getItem(COURSE_ONE));
                localStorage.setItem(COURSE_ONE, JSON.stringify(course));
            }

            window.location.href = course.webpage;
        } else {
            errors.show('slow');
            errors.text(res.error);
        }
        $("#loading").hide();
        isSearching = false;
    });
}

function setCourses(courseOne, courseTwo) {
    var $courseOne = $("#course-one");
    var $courseTwo = $("#course-two");

    if(courseOne) {
        $courseOne.html('');
        $courseOne.removeClass('disabled');
        $courseOne.append('<h3>' + courseOne.code + '</h3>');
        $courseOne.append('<p>' + courseOne.name + '</p>');
        $courseOne.click(function() {
            location.href = courseOne.webpage;
        });
    }
    if(courseTwo) {
        $courseTwo.html('');
        $courseTwo.removeClass('disabled');
        $courseTwo.append('<h3>' + courseTwo.code + '</h3>');
        $courseTwo.append('<p>' + courseTwo.name + '</p>');
        $courseTwo.click(function() {
            location.href = courseTwo.webpage;
        });
    }

}