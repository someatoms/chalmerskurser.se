function newNote() {
    var error = $('#error');
    error.hide();

    var titleElem = $("#title");
    var textElem = $("#note");
    var title = titleElem.val();
    var text = textElem.val();
    titleElem.val("");
    textElem.val("");

    if (!stripTags(title) || !stripTags(text)) {
        error.show('slow');
        return;
    }
    $.post('/api/notes', {title: title, text: text}, function (json) {
        var $error = $('#error');
        try {
            var res = JSON.parse(json);
            if (res.success) {
                if (addNote(res.results)) {
                    $('#new-note-modal').modal('hide');
                } else {
                    $error.text("Oops! Is title and text filled in?");
                    $error.show('slow');
                }
            } else {
                $error.text("API: " + res.error);
                $error.show('slow');
            }


        } catch (e) {
            console.error("Error parsing news item count json");
            console.log(json);
        }
        $("#news-link").click(function () {
            localStorage.setItem(LS_NEWS_COUNT, newsCount);
        });
    });
}

function addNote(html) {
    var $elem = $(html);
    $("#note-container").prepend($elem);
    $elem.children(".remove-note-btn").click(removeNote);
    $elem.hide();
    $elem.show('slow');
    return true;
}

function format($elem, withDot) {
    var ugly = $elem.val();
    var formatted = ugly.substr(0, 1).toUpperCase() + ugly.substr(1);
    if (withDot && formatted.charAt(formatted.length - 1).match(/(\w)/)) formatted += ".";
    if (!withDot && formatted.charAt(formatted.length - 1).match(/(\W)/)) formatted = formatted.substr(0, formatted.length - 1);
    $elem.val(formatted);
}

function removeNote() {
    var note = $(this).parent(".note");
    note.hide('slow');
    var id = note.data("id");
    $.ajax({
        url: '/api/notes/' + id,
        type: 'DELETE',
        success: function (res) {
            try {
                res = JSON.parse(res);
                if (!res.success) {
                    console.error(res.error);
                    console.log(res);
                }
            } catch (e) {
                console.error("Couldn't parse json");
                console.log(res);
            }
        }
    });
}

$(document).ready(function () {
    $('#title').on('blur', function () {
        format($(this), false);
    });
    $('#note').on('blur', function () {
        format($(this), true);
    });
    $('.remove-note-btn').click(removeNote);
    $('#new-note-modal').on('shown.bs.modal', function () {
        $('#error').hide();
        $('#title').focus();
    });
});