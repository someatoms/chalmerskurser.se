/**
 * Module handling scripts for the study rooms application
 */
var Rooms = new function () {

    // Chalmers rules
    var MAX_BOOKING_COUNT = 4;
    var MAX_TIME = 14 * 24 * 60 * 60 * 1000; // 14 days

    // Settings
    var MAX_HOUR = 16;
    var MIN_HOUR = 8;
    var ROOMS_URL = 'grupprum';

    // Fields
    var picker; // Pick-a-date instance
    var pickedHour; // Most recently picked hour

    /**
     * Initialize page
     */
    $(document).ready(function () {

        initPicker();
        initKonami();
        initHistory();

        $('.update-btn').click(updateRooms);

        updateRooms();
    });

    /**
     * Makes it possible to navigate in page hierarchy with browser buttons
     */
    function initHistory() {

        window.onhashchange = function () {
            if (location.hash === '#b') {
                if (isLoggedIn()) {
                    showBookings();
                } else {
                    location.href = '/' + ROOMS_URL + '#h';
                    $('#login-modal').modal('show');
                }
            } else if (location.hash === '#h') {
                showRooms();
            }
        };

        if (location.href.indexOf(ROOMS_URL) > -1) {
            location.href = '/' + ROOMS_URL + '#h';
        }
    }

    function initKonami() {

        var keys = [];
        var konami = '38,38,40,40,37,39,37,39,66,65';

        $(document).keydown(function (e) {
            keys.push(e.keyCode);
            if (keys.toString().indexOf(konami) >= 0) {
                $('#smurf').show('fast');
                spinSmurf();
                keys = [];
            }
        });

        var $img = $("#smurf");

        function spinSmurf() {

            $speed = 0.05;
            for ($i = 0; $i <= 1440; $i++) {
                if ($i % 100 == 0) {
                    $speed = $speed + 0.2;
                }
                rotate($i, $speed, 'Y');
            }
            setTimeout(function () {
                $('#smurf').removeAttr('style');
            }, 3000);

        }

        function rotate(degree, speed, orientation) {

            $img.css({ WebkitTransform: 'rotateY(' + degree + 'deg)'});
            $img.css({ '-transform': 'rotateY(' + degree + 'deg)'});
            $img.css({ '-moz-transform': 'rotateY(' + degree + 'deg)'});

            $img.css({ '-moz-transition': speed + 's'});
            $img.css({ '-moz-transform-style': 'preserve-3d'});
            $img.css({ '-webkit-transition': speed + 's'});
            $img.css({ '-webkit-transform-style': 'preserve-3d'});
            $img.css({ '-transition': speed + 's'});
            $img.css({ '-transform-style': 'preserve-3d'});

        }
    }

    /**
     * Initialize the picker
     */
    function initPicker() {

        var today = new Date();
        var $pickerInput = $("#inputdate").pickadate({
            format: 'd mmm',
            firstDay: 1,
            min: today,
            max: new Date(today.getTime() + MAX_TIME),
            clear: false,
            today: false,
            onSet: function () {
                pickedHour = MIN_HOUR;
                updateRooms();
            }
        });

        picker = $pickerInput.pickadate('picker');
    }

    /**
     * Update the time bar with the right set of hours
     */
    function updateHours() {
        var html = '';

        var currentHour = new Date().getHours();
        var currentDay = new Date().getDate();
        var pickedDay = parseInt(picker.get('select', 'dd'));

        // If no hour has been picked, initialize default value
        if (typeof pickedHour === 'undefined') {
            if (currentDay === pickedDay) {
                pickedHour = currentHour;
            } else {
                pickedHour = MIN_HOUR;
            }
        }

        // Set picked hour to current hour if current hour is outside supported range
        if (currentDay == pickedDay) {
            if (currentHour > MAX_HOUR || currentHour < MIN_HOUR) {
                pickedHour = currentHour;
            }
        }

        // Add a new box if current hour is less than min hour and today is chosen
        if (currentDay == pickedDay && currentHour < MIN_HOUR) {
            html += '<label class="btn time-btn active">';
            html += '<input type="radio" name="options">';
            html += pad(currentHour, 2) + ':00';
            html += '</label>';
        }

        // Collect html
        for (var hour = MIN_HOUR; hour <= MAX_HOUR; hour++) {

            if (currentDay === pickedDay && hour < currentHour) {
                html += '<label class="btn time-btn disabled">';
            } else if (hour === pickedHour) {
                html += '<label class="btn time-btn active">';
            } else {
                html += '<label class="btn time-btn">';
            }

            html += '<input type="radio" name="options">';
            html += pad(hour, 2) + ':00';
            html += '</label>';
        }

        // Add a new box if current hour is more than max hour and today is chosen
        if (currentDay == pickedDay && currentHour > MAX_HOUR) {
            html += '<label class="btn time-btn active">';
            html += '<input type="radio" name="options">';
            html += pad(currentHour, 2) + ':00';
            html += '</label>';
        }


        // Set the html and click listeners
        var $hoursCont = $('.hours-cont');
        $hoursCont.unbind().html(html).children().not('.disabled').click(function () {
            pickedHour = parseInt($(this).text());
            updateRooms();
        });

        // Scroll right if picked hour is more than 12
        if (pickedHour > 12) {
            $hoursCont.scrollLeft(400);
        }
    }

    /**
     * Fetch availble rooms and bookings from TimeEdit
     */
    function updateRooms() {
        updateHours();
        toggleLoading(true);
        var hour = parseInt($('.timebar').find('.time-btn.active').text());
        var date = picker.get('select', 'yyyymmdd');
        $('#buildings').load('/api/' + ROOMS_URL + '?date=' + date + '&hour=' + hour, function (response, status) {
            if (status === 'error') {
                showToast("An error occured, couldn't fetch rooms");
                toggleLoading(false);
                return;
            }
            $('#bookings').load('/api/bookings', function (response2, status2, request2) {
                if (status2 === 'error') {
                    showToast('An error occured, couldn\'t fetch bookings');
                    toggleLoading(false);
                    return;
                }
                addEventHandlersBookings();

                toggleLoading(false);
            });
            $('.room').click(showBooking);
        });
    }

    /**
     * After update, add event handlers to the dom again.
     */
    function addEventHandlersBookings() {
        $('.btn-confirmed-cancel').click(cancelBooking);
        $('.btn-confirm').click(createBooking);
        $('#login-form').submit(login);

        $('.btn-unconfirmed-cancel').click(function () {
            $(this).parent().parent().removeClass('unconfirmed');
            $(this).parent().parent().addClass('empty');
        });

        $('.btn-inc').click(function () {
            var $dur = $(this).siblings('.duration');
            var val = parseInt($dur.text());
            if (val < MAX_BOOKING_COUNT) {
                $dur.text((val + 1) + 'h');
            }
        });

        $('.btn-dec').click(function () {
            var $dur = $(this).siblings('.duration');
            var val = parseInt($dur.text());
            if (val > 1) {
                $dur.text((val - 1) + 'h');
            }
        });
    }

    /**
     * Sends a login requests to the server
     * @param evt
     */
    function login(evt) {
        evt.preventDefault();
        evt.stopPropagation();

        var cid = $('#cid').val();
        var pass = $('#password').val();

        if (cid.length < 2 || pass.length < 2) {
            showToast('Fyll i CID och lösenord');
            return;
        }

        toggleLoading(true);

        $.post('/api/grupprum/login', {cid: cid, password: pass}, function (data) {
            if (data.success) {
                $('#bookings').html(data.html);
                addEventHandlersBookings();
                loggedIn = true;
                $('#login-modal').modal('hide');
                showToast('Du är nu inloggad!');
            } else {
                $('#login-modal').find('.alert').show('fast').text(data.message);

            }
            toggleLoading(false);
        });
    }

    /**
     * Send a logout request
     */
    this.logout = function () {
        toggleLoading(true);
        $.post('/api/rooms/logout', function (data) {
            if (data.success) {
                location.href = '/' + ROOMS_URL + '#h';
                $('#bookings').html('');
                loggedIn = false;
                showToast('Du är nu utloggad.');
            } else {
                showToast("Couldn't logout.");
            }
            toggleLoading(false);
        });
    };

    /**
     * Send a cancel request
     */
    function cancelBooking() {
        toggleLoading(true);
        var id = $(this).parents('.booking').data('id');
        $.ajax({
            url: '/api/bookings/' + id,
            type: 'DELETE',
            success: function (result) {
                if (result === '') {
                    updateRooms();
                } else {
                    showToast(result);
                }
            }
        });
    }

    /**
     * Send a create request
     */
    function createBooking() {
        toggleLoading(true);
        var date = picker.get('select', 'yyyymmdd');
        var startHour = parseInt($('.timebar').find('.time-btn.active').text());
        var duration = parseInt($('.booking .duration').text());
        var endHour = startHour + duration;
        var roomId = $(this).parents('.booking').data('roomId');

        var data = {date: date, startHour: startHour, endHour: endHour, roomId: roomId};
        var url = '/api/createBooking';

        $.get(url, data, function (result) {
            if (result.success) {
                updateRooms();
            } else {
                showToast(result.message);
                toggleLoading(false);
            }
        });
    }

    /**
     * Shows a toast
     * @param str The string to display
     */
    function showToast(str) {
        $('#toast').text(str).fadeIn('fast');
        setTimeout(function () {
            $('#toast').fadeOut('fast');
        }, 4000);
    }

    /**
     * Toggle if the loading icon should be spinning
     * @param bool
     */
    function toggleLoading(bool) {
        $('.update-btn').toggleClass('spin', bool);

    }

    /**
     * Show the bookings page
     */
    function showBookings() {
        $('#user-section').show();
        $('#room-section').hide();
    }

    this.showBookings = function () {
        if (isLoggedIn()) {
            location.href = '/' + ROOMS_URL + '#b';
        } else {
            $('#login-modal').modal('show');
        }
    };

    /**
     * Show the study room page
     */
    this.showRooms = function () {
        location.href = '/' + ROOMS_URL + '#h';
    };

    function showRooms() {
        $('#user-section').hide();
        $('#room-section').show();
    }

    /**
     * Add a booking to the my bookings page and go there.
     */
    function showBooking(room) {
        if (typeof bookingsCount === 'undefined') {
            if (isLoggedIn()) {
                showToast('Still loading...');
            } else {
                $('#login-modal').modal('show');
            }
            return;
        }

        if (bookingsCount < MAX_BOOKING_COUNT) {

            window.scrollTo(0, 0);
            location.href = '/' + ROOMS_URL + '#b';

            var number = $(this).find('.number').text();
            var info = $(this).find('.info').text();
            var roomId = $(this).data('id');
            var selector = '#booking' + (bookingsCount + 1);

            var freeTime = parseInt($(this).find('.free-time').text());

            var $cont = $(selector);
            $cont.data('roomId', roomId);
            $cont.attr('class', 'booking unconfirmed');
            $cont.find('.duration').text(freeTime + 'h');
            $cont.find('.info').text(info);
            $cont.find('h5').text(number);

        } else {
            showToast('You have booked the maximum amount of rooms');
        }
    }

    function isLoggedIn() {
        return loggedIn;
    }

};