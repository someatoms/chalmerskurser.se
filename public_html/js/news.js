var LS_NEWS_COUNT = "read_news_count";

$(document).ready(function () {
    initNewsBadge();
});

/**
 * Initializes the news badge to show if unread news.
 */
function initNewsBadge() {
    if(!lsTest()) {
        return;
    }
    var badge = $("#unread-news-badge");
    badge.hide();
    $.get("/api/news", {}, function (json) {
        var newsCount = 0;
        var res;
        try {
            res = JSON.parse(json);
            newsCount = res.results;
            if (getReadNewsCount() < newsCount) {
                showNewsBadge(newsCount);
            } else if (getReadNewsCount() === newsCount) {
                // Do nothing, badge already hidden
            } else {
                localStorage.removeItem(LS_NEWS_COUNT);
                showNewsBadge(newsCount);
                console.error("Unexpected news count value saved");
            }
        } catch (e) {
            console.error("Error parsing news item count json");
            console.log("json");
        }
        $("#news-link").click(function () {
            localStorage.setItem(LS_NEWS_COUNT, newsCount);
        });
    });

    function showNewsBadge(newsCount) {
        var badge = $("#unread-news-badge");
        badge.show('slow');
        badge.text(newsCount - getReadNewsCount());
    }
}

/**
 * Get how many news items the current user has seen
 * @returns number Zero or more
 */
function getReadNewsCount() {
    var readNewsCount = parseInt(localStorage.getItem(LS_NEWS_COUNT), 10);
    // Return 0 if NaN or some other unexpected value
    if (readNewsCount > 0) {
        return readNewsCount;
    } else {
        return 0;
    }
}

function subscribe() {
    var email = $("#subscribe-email").val();
    var $status = $("#subscribe-status");
    $status.hide();
    if (!isEmail(email)) {
        $status.text("Not an email");
        $status.show('slow');
        return;
    }
    $.ajax({
        url: '/api/subscribers/' + decodeURIComponent(email),
        type: 'POST',
        success: function (json) {
            try {
                var res = JSON.parse(json);
                if (res.success) {
                    $status.text("Subscribed!");
                } else {
                    $status.text("API: " . res.error);
                }
            } catch (e) {
                $status.text("Couldn't subscribe");
                console.error("Couldn't parse json");
                console.log(json);
            }
            $status.show('slow');
        }
    });
}
