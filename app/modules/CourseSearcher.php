<?php

class CourseSearcher
{
    const BASE_URL = "http://www.student.chalmers.se/sp/course_list";
    const COURSE_TABLE_INDEX = 15;
    const COURSE_ROW_INDEX = 2;

    /**
     * Get the course with the given code
     * @param $searchTerm string
     * @throws Exception When search didn't match any courses or courses could not be retreived for other reason
     * @return Course[]
     */
    public static function search($searchTerm)
    {
        if(empty($searchTerm)) {
            throw new Exception("No search term specified");
        }
        $searchTerm = strtoupper($searchTerm);
        $queryString = "?flag=1&search_course_code=$searchTerm";
        $baseDoc = self::getBaseDoc($queryString);
        $courseTr = self::getCourseRow($baseDoc, $searchTerm);
        if ($courseTr === null) {
            throw new Exception("No courses found");
        } else {
            return array(self::createCourse($courseTr));
        }
    }

    /**
     * @param $courseTr
     * @return DOMDocument
     */
    private static function createCourse($courseTr)
    {
        // Sort out td elements from table row
        $course = array();
        foreach ($courseTr->childNodes as $td) {
            if ($td->nodeName === 'td') {
                $course[] = $td;
            }
        }

        // Extract wanted course info
        $code = $course[0]->nodeValue;
        $name = $course[1]->nodeValue;
        $points = $course[2]->nodeValue;
        $insti = $course[3]->nodeValue;
        $portal = self::BASE_URL . "/" . $course[4]->childNodes->item(0)->attributes->item(0)->nodeValue;
        $webpage = $course[5]->childNodes->item(1);
        if($webpage) {
            $webpage = $webpage->attributes->item(0)->nodeValue;
        }

        if($code === "TDA416") {
            $webpage = "http://tda416.simonbengtsson.se";
        }

        return new Course($code, $name, $webpage, $portal, $insti, $points);
    }

    /**
     * @param $baseDoc DOMDocument
     * @param $code string
     * @return DOMNode|null
     */
    private static function getCourseRow($baseDoc, $code)
    {
        $tables = $baseDoc->getElementsByTagName("table"); // All tables on page
        $courseTable = $tables->item(self::COURSE_TABLE_INDEX); // The table with course info
        $courseTr = $courseTable->childNodes->item(self::COURSE_ROW_INDEX); // Table row with course info

        if ($courseTr->childNodes->item(0)->nodeValue === $code) {
            return $courseTr;
        } else {
            return null;
        }
    }

    /**
     * @param $queryString
     * @return DOMDocument
     */
    private static function getBaseDoc($queryString = "")
    {
        $url = self::BASE_URL . $queryString;
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTMLFile($url);
        libxml_clear_errors();
        return $doc;
    }

}