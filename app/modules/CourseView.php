<?php

/**
 * Class View
 * Contains helper methods for displaying pages to user
 */
class CourseView
{
    /**
     * Fetches lecturer and assist information
     * @return string
     */
    public static function getLecturerHtml()
    {
        try {
            $html = file_get_html('http://www.cse.chalmers.se/edu/course/tda416/courseInfo/lecturers.html');
        } catch (Exception $e) {
            return "Can't read official course web page. Is it down?";
        }
        foreach ($html->find('img') as $img) {
            $img->outertext = '';
        }
        foreach ($html->find('dt, h3') as $element) {
            $element->attr['style'] = '';
        }
        foreach ($html->find('a') as $a) {
            $href = $a->href;
            if (strpos($href, 'http') === false && strpos($href, 'mailto') === false) {
                $a->href = "http://www.cse.chalmers.se/edu/course/tda416/courseInfo/" . $href;
            }
        }
        $str = $html;
        $arr = explode("<dl>", $str);
        $lecturerHTML = explode("<hr width=\"100%\">", $arr[1]);
        return $lecturerHTML[0];
    }

    /**
     * Fetches html from the course webpage for the requested page name. Returns a 404 string if the
     * page name is not valid.
     * @param $pageName string Can be "exams", "news", "lecture-notes" or "assignments".
     * @return bool|mixed|simple_html_dom|string
     */
    public static function getPageHTML($pageName)
    {
        if ($pageName === 'news') {
            return self::getNewsHtml();
        } elseif ($pageName === 'lecture-notes') {
            return self::getLectureNoteHtml();
        } elseif ($pageName === 'exams') {
            return self::getExamHtml();
        } elseif ($pageName === 'assignments') {
            return self::getAssignmentHtml();
        } else {
            return "<h4>Oops! This is a 404.</h4>";
        }
    }

    private static function getNewsHtml()
    {
        $news = "<h3>News</h3>";
        try {
            $html = file_get_html('http://www.cse.chalmers.se/edu/course/tda416/announcements/senaste.html', false,
                null, -1, -1, true, true, DEFAULT_TARGET_CHARSET, false);
        } catch (Exception $e) {
            return "Can't read official course web page. Is it down?";
        }

        $str = $html;
        $str = explode("<body bgcolor=\"#FFFFFF\" marginwidth=\"10\" marginheight=\"10\">", $str);
        $str = $str[1];
        $str = explode("</body>", $str);
        $str = $str[0];
        $arr = explode("<h2>Permanent links</h2>", $str);
        $news .= $arr[0];
        $news = preg_replace('/<!--(.*)-->/Uis', '', $news);

        return $news;
    }

    private static function getLectureNoteHtml()
    {
        try {
            $html = file_get_html('http://www.cse.chalmers.se/edu/course/tda416/courseMtrl/timeline.html');
        } catch (Exception $e) {
            return "Can't read official course web page. Is it down?";
        }
        foreach ($html->find('a') as $a) {
            $href = $a->href;
            $a->href = "http://www.cse.chalmers.se/edu/course/tda416/courseMtrl/" . $href;
        }
        return (string)$html;


    }

    private static function getExamHtml()
    {
        try {
            $html = file_get_html('http://www.cse.chalmers.se/edu/course/tda416/courseMtrl/exams.html');
        } catch (Exception $e) {
            return "Can't read official course web page. Is it down?";
        }
        foreach ($html->find('a') as $a) {
            $href = $a->href;
            $a->href = "http://www.cse.chalmers.se/edu/course/tda416/courseMtrl/" . $href;
        }
        return (string)$html;
    }

    private static function getAssignmentHtml()
    {
        try {
            $html = file_get_html('http://www.cse.chalmers.se/edu/course/tda416/assignments/flik4.textram.html');
        } catch (Exception $e) {
            return "Can't read official course web page. Is it down?";
        }
        foreach ($html->find('a') as $a) {
            $href = $a->href;
            if (strpos($href, 'http') === false) {
                $a->href = "http://www.cse.chalmers.se/edu/course/tda416/courseMtrl/" . $href;
            }
        }
        $arr = explode("<h3>Submitting instructions </h3>", $html);
        return (string)$arr{0};
    }
} 