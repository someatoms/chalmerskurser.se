<?php

/**
 * Class Util
 */
class Util
{
    /**
     * Counts how many news items there are on the official news page.
     * @return int
     */
    public static function countNews()
    {
        $newsPageHtml = CourseView::getPageHTML("news");
        // Every <dt> found on page is one news item (2014-02-09).
        // This might might change in the future though.
        $occurrences = substr_count($newsPageHtml, "<dt>");
        return $occurrences;
    }

    /**
     * Reads all watchers from file
     * @return string[] An array with emails
     */
    public static function readWatchers()
    {
        if (!file_exists(Vars::$watchersFile)) {
            return array();
        }
        $emails = json_decode(file_get_contents(Vars::$watchersFile));
        if (!is_array($emails)) {
            return array();
        }
        return $emails;
    }

    /**
     * Save the email to a list of news watchers
     * @param $email string
     * @return bool success
     */
    public static function addNewsWatcher($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            var_dump($email);
            return false;
        }
        $emails = Util::readWatchers();
        if (in_array($email, $emails)) {
            return true;
        }
        $emails[] = $email;
        $json = json_encode($emails);
        file_put_contents(Vars::$watchersFile, $json);
        return true;
    }

    /**
     * Save the email to a list of news watchers
     * @param $email string
     * @return bool
     */
    public static function removeNewsWatcher($email)
    {
        $emails = Util::readWatchers();
        $key = array_search($email, $emails);
        if ($key !== false) {
            unset($emails[$key]);
            $json = json_encode($emails);
            file_put_contents(Vars::$watchersFile, $json);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Clean the string with HTMLPurifier. It removes dangerous html tags but spares most.
     * @param $dirty
     * @return string Clean string
     */
    public static function clean($dirty)
    {
        $purifier = new HTMLPurifier();
        return $purifier->purify($dirty);
    }

    /**
     * Reads all notes from saved file and cleans title and text just in case.
     * @return Note[] An array with Notes
     */
    public static function getSavedNotes()
    {
        if (!file_exists(Vars::$notesFile)) {
            return array();
        }
        $jsonNotes = json_decode(file_get_contents(Vars::$notesFile));
        if (!is_array($jsonNotes)) {
            return array();
        }
        $notes = array();
        foreach ($jsonNotes as $jsonNote) {
            $title = self::clean($jsonNote->title);
            $text = self::clean($jsonNote->text);
            $note = new Note($title, $text, $jsonNote->id, $jsonNote->time);
            array_push($notes, $note);
        }
        return $notes;
    }

    /**
     * Save the specified note to file
     * @param $note Note
     */
    public static function saveNote($note)
    {
        $notes = Util::getSavedNotes();
        array_unshift($notes, $note);
        $json = json_encode($notes);
        File::put(Vars::$notesFile, $json);
        $note->makeRemovable();
    }

    /**
     * Remove the specified note
     * @param $noteId
     * @return bool
     */
    public static function removeNote($noteId)
    {
        $notes = Util::getSavedNotes();
        $newNotes = array();
        foreach ($notes as $note) {
            if ($note->id !== $noteId) {
                array_push($newNotes, $note);
            }
        }

        $json = json_encode($newNotes);
        file_put_contents(Vars::$notesFile, $json);

        return count($notes) - count($newNotes) === 1;
    }

    public static function checkNews()
    {
        if (self::countNews() > self::getLastNewsCount()) {
            self::notifyWatchers();
            self::updateLastNewsCount();
            return true;
        } else {
            self::updateLastNewsCount(); // Update in case of tmp data saved during testing
            return false;
        }
    }

    private static function getLastNewsCount()
    {
        $others = self::readJSONFile(Vars::$othersFile);
        if (isset($others[Vars::$LAST_NEWS_COUNT_NAME]) && is_numeric($others[Vars::$LAST_NEWS_COUNT_NAME])) {
            return $others[Vars::$LAST_NEWS_COUNT_NAME];
        } else {
            return 0;
        }
    }

    private static function readJSONFile($file)
    {
        if (!file_exists($file)) {
            return array();
        }
        $array = json_decode(file_get_contents($file), true);
        if (!is_array($array)) {
            return array();
        }
        return $array;
    }

    private static function notifyWatchers()
    {
        $success = true;
        foreach (self::readWatchers() as $watcher) {
            $subject = "News for TDA416";
            $msg = CourseView::getPageHTML("news");
            $msg .= "<a href='$_SERVER[HTTP_HOST]/tda416/?unsubscribe=$watcher'>Unsubscribe for TDA416 news updates</a>";
            $simon = "simon@simonbengtsson.se";

            $headers = array();
            $headers[] = "MIME-Version: 1.0";
            $headers[] = "Content-type: text/html; charset=utf-8";
            $headers[] = "From: TDA416 <$simon>";
            $headers[] = "Reply-To: Simon Bengtsson <$simon>";
            $headers[] = "Subject: {$subject}";
            $headers[] = "X-Mailer: PHP/" . phpversion();

            $res = mail($watcher, $subject, $msg, implode("\r\n", $headers));
            if (!$res) $success = false;
        }
        return $success;
    }

    private static function updateLastNewsCount()
    {
        $others = self::readJSONFile(Vars::$othersFile);
        $others[Vars::$LAST_NEWS_COUNT_NAME] = self::countNews();
        $json = json_encode($others);
        file_put_contents(Vars::$othersFile, $json);
    }
}