<?php

/**
 * Class Vars
 * Contains various variables used in the application
 */
class Vars {
    public static $version = "0.1 Beta";
    public static $notesFile;
    public static $watchersFile;
    public static $othersFile;
    public static $removablesKey = "removable_cookie";
    public static $LAST_NEWS_COUNT_NAME = "last_news_count_name";
}

Vars::$notesFile = app_path() . "/storage/data-notes.dat";
Vars::$watchersFile = app_path() . "/storage/data-watchers.dat";
Vars::$othersFile = app_path() . "/storage/data-others.dat";