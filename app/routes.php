<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('home.home', array('title' => 'Hitta kurshemsidor på Chalmers'));
});
Route::any('/api/{list}/{item?}', 'APIController@handleAPIRequest');

Route::any('/grupprum', function() {
    return Redirect::to('https://timeedit.chalmerskurser.se');
});

Route::any('/{code}', 'CourseController@showCourse');

