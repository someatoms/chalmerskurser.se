<?php

/**
 * @author Simon Bengtsson
 */
class Booking
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $time;

    /**
     * @var Room
     */
    public $room;

    public function __construct($id, $room, $time) {
        $this->id = $id;
        $this->room = $room;
        $this->time = $time;
    }
}