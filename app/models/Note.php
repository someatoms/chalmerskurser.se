<?php

/**
 * Class Note
 * Represents one note that the user can create.
 */
class Note
{
    const HOURS_REMOVABLE = 48;
    public $id;
    public $title;
    public $text;
    public $time;

    /**
     * Creates a new note. If $id is null a new is generated. If $time is null it is set to now.
     * @param string $title
     * @param string $text
     * @param string $id
     * @param number $time
     */
    public function __construct($title = null, $text = null, $id = null, $time = null)
    {
        $this->title = $title;
        $this->text = $text;
        if ($id === null) {
            $this->id = $this->generateId();
        } else {
            $this->id = $id;
        }
        if ($time === null) {
            $this->time = time();
        } else {
            $this->time = $time;
        }
    }

    /**
     * Generates a five character random string id
     * @return string
     */
    private function generateId()
    {
        return substr(md5(uniqid()), 0, 5); // TODO: Increase id length
    }

    /**
     * Get an html representation of this note
     * @param bool $tmpRemovable
     * @internal param \Note $note note
     * @return string
     */
    public function getHtml($tmpRemovable = false)
    {
        $html = "";
        if ($tmpRemovable || self::isRemovable($this->id)) {
            $html .= "<div class='note' data-id='$this->id'>";
            $html .= "<span class='glyphicon glyphicon-remove-circle remove-note-btn'></span>";
        } else {
            $html .= "<div class='note'>";
        }
        $html .= "<div class='date'><span>" . date("Y-m-d", $this->time) . "</span></div>";
        $html .= "<h4>$this->title</h4>";
        $html .= "<p>$this->text</p>";
        $html .= "</div>";
        return $html;
    }

    /**
     * Checks if the current user are allowed to remove this note
     * @return bool
     */
    private function isRemovable()
    {
        if (Session::get(Vars::$removablesKey)) {
            return true;
        }
        return false;
    }

    /**
     * Adds this note to a cookie with notes that the current user are allowed to remove
     */
    public function makeRemovable()
    {
        Session::push(Vars::$removablesKey, $this->id);
    }
}