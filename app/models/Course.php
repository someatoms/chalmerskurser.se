<?php

class Course {

    public $code;
    public $name;
    public $points;
    public $institution;
    public $webpage;
    public $portal;

    function __construct($code, $name = null, $courseWebpage = null, $portalWebpage = null, $institution = null, $points = null)
    {
        $this->code = $code;
        $this->webpage = $courseWebpage;
        if($this->webpage === null) {
            unset($this->webpage);
        }
        $this->institution = $institution;
        $this->name = $name;
        $this->points = $points;
        $this->portal = $portalWebpage;
    }

} 