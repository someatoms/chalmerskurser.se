<?php

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class TimeEdit
 */
class TimeEdit
{
    const MAX_BOOKING_TIME = 4;
    const URL_ENCODED = 'application/x-www-form-urlencoded; charset=utf-8';
    const ALL_ROOMS_URL = 'https://se.timeedit.net/web/chalmers/db1/b1/objects.html?fr=t&partajax=t&im=f&add=f&sid=1002&l=sv_SE&step=1&grp=5&types=186';

    /**
     * @var DateTime
     */
    private static $date;


    /**
     * Used when updating default room info
     * @return mixed
     */
    public function fetchAllRooms()
    {
        $client = $this->login(self::ALL_ROOMS_URL);
        $crawler = $client->getCrawler()->filter('.searchObject');
        $roomInfoArr = $this->getInfoFromRoomList($crawler);

        $rooms = array();
        foreach ($roomInfoArr as $roomInfo) {
            $infoUrl = 'https://se.timeedit.net/web/chalmers/db1/b1/objects/' . $roomInfo['id'] . '/o.json?fr=t&sid=1002';
            $client->request('GET', $infoUrl);
            $json = $client->getResponse()->getContent();
            $roomInfo2 = json_decode($json, true);

            $roomSizeUrl = 'https://se.timeedit.net/web/chalmers/db1/b1/objects/' . $roomInfo['id'] . '.json';
            $client->request('GET', $roomSizeUrl);
            $json = $client->getResponse()->getContent();
            $values = json_decode($json, true)['records'];
            if(sizeof($values) === 0) {
                dd($roomInfo);
                continue;
            }
            $values = $values[0]['values'];
            $exploded = explode(',', $values);
            $roomSize = intval($exploded[4]);
            if($roomSize === 0 && sizeof($exploded) > 5) {
                $roomSize = intval(explode(',', $values)[5]);
            }
            if($roomSize === 0 && sizeof($exploded) > 6) {
                $roomSize = intval(explode(',', $values)[6]);
            }
            $rooms[$roomInfo['id']] = new Room($roomInfo['id'], $roomInfo['name'], $roomInfo2['Lokaltyp'],
                $roomInfo2['Byggnad'], $roomInfo2['Utrustning'], $roomSize);
        }
        return $rooms;
    }

    public function fetchAvailableRooms($date, $hour, $duration)
    {
        $url = 'https://se.timeedit.net/web/chalmers/db1/b1/objects.html?partajax=t&sid=1002&step=1&types=186&dates=';
        $url .= $date . '-' . $date . '&starttime=' . $hour . '%3A00&endtime=' . ($hour + $duration) . '%3A00';

        $client = $this->login($url);
        $crawler = $client->getCrawler()->filter('.searchObject');
        $roomInfoArr = $this->getInfoFromRoomList($crawler);

        $availableRooms = array_where(Room::all(), function ($roomId, $room) use ($roomInfoArr) {
            foreach ($roomInfoArr as $roomInfo) {
                if (('' . $roomId) === $roomInfo['id']) {
                    return true;
                }
            }
            return false;
        });

        return $availableRooms;
    }

    public function cancelBooking($id)
    {
        $url = 'https://se.timeedit.net/web/chalmers/db1/b1';
        $client = $this->login($url);

        $deleteUrl = 'https://se.timeedit.net/web/chalmers/db1/b1/r.html?id=' . $id;
        $client->request('DELETE', $deleteUrl);
        $result = $client->getResponse()->getContent();

        return $result;
    }

    public function createBooking($date, $startHour, $endHour, $roomId)
    {
        $url = 'https://se.timeedit.net/web/chalmers/db1/b1';
        $client = $this->login($url);

        $data = 'dates=' . $date . '&datesEnd=' . $date;
        $data .= '&startTime=' . $startHour . '%3A00&endTime=' . $endHour . '%3A00';
        $data .= '&o=' . $roomId . '.186&o=203460.192';

        $data .= '&url=https%3A%2F%2Fse.timeedit.net%2Fweb%2Fchalmers%2Fdb1%2Fb1%2Fr.html';
        $data .= '%3Fh%3Dt%26sid%3D1002%26id%3D-1%26step%3D2%26id%3D-1%26';
        $data .= 'dates%3D' . $date . '%26datesEnd%3D' . $date . '%26';
        $data .= 'startTime%3D' . $startHour . '%253A00%26endTime%3D' . $endHour . '%253A00%26';
        $data .= 'o%3D' . $roomId . '.186%252C5215%26o%3D203460.192%252C%25C3%2596vrigt'; // Consider fixing this line

        $client->removeHeader('Expect');
        $client->setHeader('Content-Type', self::URL_ENCODED);

        $url = 'https://se.timeedit.net/web/chalmers/db1/b1/r.html';

        $crawler = $client->request('POST', $url, array(), array(), array(), $data);

        $result = $client->getResponse()->getContent();

        return $result;
    }

    /**
     * Logins to TimeEdit and returns a Client object at the requested url. Uses credentials in this order:
     *
     * 1. The $user and $pass parameters.
     * 2. Session credentials
     * 3. Default credentials
     *
     * @param $url string
     * @param null $user
     * @param null $pass
     * @return Client
     */
    private function login($url, $user = null, $pass = null)
    {
        if (!$user || !$pass) {
            $user = Session::get('cid');
            $pass = Session::get('password');

            if (!$user || !$pass) {
                $user = $_ENV['defaultChalmersUser'];
                $pass = $_ENV['defaultChalmersPass'];
            }
        }


        $client = new Client();
        $crawler = $client->request('GET', $url);
        $form = $crawler->selectButton('Logga in')->form();
        $client->submit($form, array('username' => $user, 'password' => $pass));

        return $client;
    }

    public function isLoggedIn()
    {
        return Session::get('cid') && Session::get('password');
    }

    /**
     * From a list of rooms, extract relevant info
     * @param Crawler $crawler A Crawler pointing to a room list
     * @return array Array with room id, name and type
     */
    private function getInfoFromRoomList(Crawler $crawler)
    {
        $roomInfoArr = $crawler->each(function (Crawler $node, $i) {
            return array(
                'id' => $node->attr('data-idonly'),
                'name' => $node->attr('data-name'),
                'type' => $node->attr('data-type'));
        });
        return $roomInfoArr;
    }

    public function getBookings()
    {
        $url = 'https://se.timeedit.net/web/chalmers/db1/b1/myreservations.html?so=100&part=t&sid=1002';
        $crawler = $this->login($url)->getCrawler();
        $crawler = $crawler->filter('.clickable2');

        $bookings = $crawler->each(function (Crawler $node, $i) {
            $id = $node->attr('data-id');

            $roomText = $node->filter('.column0')->text();
            $roomName = explode(',', $roomText)[0];
            $room = Room::findFromName($roomName);

            $timeStr = $node->filter('.time')->text();
            $timeArr = explode(' ', $timeStr);

            // If the first time string is an empty string, use the last recorded date instead
            if ($timeArr[0]) {
                self::$date = new DateTime($timeArr[0]);
            }
            $time = self::$date->format('M j') . ' ' . $timeArr[2] . '-' . $timeArr[4];

            return new Booking($id, $room, $time);
        });

        return $bookings;
    }

    /**
     * Attempts login and saves credentials in Session if successful.
     *
     * @param $cid
     * @param $pass
     * @return bool|string True if successful, otherwise, the error message.
     */
    public function saveLogin($cid, $pass)
    {

        $url = 'https://se.timeedit.net/web/chalmers/db1/b1';
        $client = $this->login($url, $cid, $pass);
        $crawler = $client->getCrawler()->filter('.loginbox');

        if ($crawler->count() !== 0) {
            $message = $crawler->filter('#message')->text();
            $others = $crawler->filter('#message span')->text();
            return str_replace($others, '', $message);
        }

        Session::put('cid', $cid);
        Session::put('password', $pass);

        return true;
    }
}

?>
