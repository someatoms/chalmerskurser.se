<?php

use \Symfony\Component\DomCrawler\Crawler;

/**
 * @author Simon Bengtsson
 */
class Room
{
    public $id;
    public $name;
    public $type;
    public $building;
    public $equipment;
    public $size;
    public $freeTime;

    function __construct($id, $name, $type, $building, $equipment, $size)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->building = $building;
        $this->equipment = $equipment;
        $this->size = $size;
    }

    /*
	|--------------------------------------------------------------------------
	| Static room array section
	|--------------------------------------------------------------------------
	*/

    const FILE_PATH = '../app/storage/rooms.dat';

    private static $rooms = array();

    public static function find($id) {
        return self::$rooms[$id];
    }

    public static function findFromName($name)
    {
        $room = array_first(self::$rooms, function($key, $room) use ($name) {
            return $room->name === $name;
        });
        return $room;
    }

    public static function all() {
        return self::$rooms;
    }

    /**
     * Saves the given rooms to file
     * @param $rooms array The rooms to save
     */
    public static function saveRooms($rooms)
    {
        $str = serialize($rooms);
        file_put_contents(Room::FILE_PATH, $str);
        self::loadRooms();
    }

    /**
     * Loads rooms from file
     */
    public static function loadRooms()
    {
        $str = file_get_contents(Room::FILE_PATH);
        self::$rooms = unserialize($str);
    }
}

/**
 * Load rooms when the application starts up.
 */
Room::loadRooms();