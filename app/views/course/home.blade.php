@extends('layouts.course')
@section('content')
<?php
/**
 * Course Home
 * View that displays the course home page. Consists of user/student notes and some extra information.
 */
?>

<div class="container" style="padding: 0;">
    <div class="col-md-8">
        <h1>Notes</h1>
        <!-- Button trigger modal -->
        <button id="new-note" class="btn btn-primary btn-sm" style="margin-bottom: 20px;" data-toggle="modal"
                data-target="#new-note-modal">
            <span class="glyphicon glyphicon-plus-sign" style="margin-right: 5px;"></span>
            Post new note
        </button>

        <!-- Modal -->
        <div class="modal fade" id="new-note-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Post a new note</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title">
                        </div>
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea class="form-control" id="note"></textarea>
                            <p class="help-block" style=""><em>Most html tags allowed</em></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span id="error" style="display: none; margin-right: 10px; font-weight: bold;">
                            Oops! Is title and text set?
                        </span>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" onclick="newNote()">Post note</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="note-container">
            <?php
            foreach (Util::getSavedNotes() as $note) {
                echo $note->getHtml();
            }
            ?>
            <div class="note">
                <div class="date"><span>2014-02-07</span></div>
                <h4>Interactive datastructure animations</h4>

                <p>"The best way to understand complex data structures is to see them in action"</p>
                <a href="http://www.cs.usfca.edu/~galles/visualization/SplayTree.html">Splay
                    Tree</a><br>
                <a href="http://www.cs.usfca.edu/~galles/visualization/Algorithms.html">Others</a>
            </div>
            <div class="note">
                <div class="date">
                    <span>2014-02-07</span>
                </div>
                <h4>Alternative course book</h4>
                <p>
                    <a href="http://ag.mycel.nl/algo/schoolboek-data_structures_and_algorithms_in_java.pdf">
                        Data Structures &amp; Algorithms in Java
                    </a>
                    by Robert Lafore is a great alternative course book.
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <h1>Information</h1>

        <div class="extra">
            <p>The information here is fetched from the official TDA416 course website. The idea is to present it in a
                prettier (the official doesn't have pink) and mobile optimized way.</p>
            <p><a href="http://www.cse.chalmers.se/edu/course/tda416/">Official course website</a></p>
            <p><a href="https://fire.cs.chalmers.se:8021/cgi-bin/Fire">Fire submission system</a></p>
            <p style="font-size: 12px; font-style: italic;">When signing up to fire, don't forget that Erland is happy
                when people fill in there SSN on the format <strong>920505-xxxx</strong>.</p>
        </div>
        <div class="extra">
            <h4>News updates</h4>

            <p>Fill in your email to get notified if any news gets posted on the official course webpage.</p>

            <div class="form-group">
                <input id="subscribe-email" class="form-control" placeholder="Email">
            </div>
            <button style="margin-top: 10px;" class="btn btn-primary" onclick="subscribe()">Subscribe</button>
            <div style="display: inline-block; vertical-align: bottom; margin-bottom: -7px; margin-left: 10px;">
                <p style="display: none; color: #777; font-weight: bold; font-size: 16px;" id="subscribe-status"></p>
            </div>
        </div>
    </div>

</div>
@stop
