<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#main-links">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand">
                <a href="/" target="_self" class="go-home">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a target="_self" href="/{{strtolower($course->code)}}" class="brand">
                    <img src="/assets/graph.png" alt="graph">
                    <span>Data structures TDA416</span>
                </a>
            </div>

        </div>

        <div class="collapse navbar-collapse" id="main-links">
            <ul class="nav navbar-nav navbar-right">
                <li><a target="_self" href="/{{strtolower($course->code)}}">Home</a></li>
                <li><a target="_self" id="news-link" href="/{{strtolower($course->code)}}/?page=news">
                        News
                        <span id="unread-news-badge" class='badge'>4</span>
                    </a></li>
                <li><a target="_self" href="/{{strtolower($course->code)}}/?page=assignments">Assignments</a></li>
                <li><a target="_self" href="/{{strtolower($course->code)}}/?page=exams">Exams</a></li>
                <li><a target="_self" href="/{{strtolower($course->code)}}/?page=lecture-notes">Lecture notes</a></li>
            </ul>
        </div>
    </div>
</nav>