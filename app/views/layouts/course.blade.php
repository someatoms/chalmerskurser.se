<!DOCTYPE html>

<html>
<head>
@include('includes.head')
</head>
<body id="course">

@include('course.header')
@yield('content')
@include('includes.scripts')

</body>
</html>