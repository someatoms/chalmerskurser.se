<!DOCTYPE html>

<html>
<head>
@include('includes.head')
</head>
<body id="home">

@yield('content')
@include('includes.scripts')

</body>
</html>