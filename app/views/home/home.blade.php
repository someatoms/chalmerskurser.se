@extends('layouts.home')
@section('content')
<div id="sidebar">
    <h1>Chalmers Kurser</h1>

    <p>
        Här kan du söka efter kurshemsidor på Chalmers. Om en kurshemsida har en omgjord version
        kommer den att länkas, annars den officiella.
    </p>

    <div id="news">
        <h3>Nytt</h3>

        <p>Alla kurser är nu sökbara via Chromes omnibar och chalmerskurser.se/{kurskod}</p>
        <a href="/dat026" target="_blank">
            <button id="try" class="button">Prova här!</button>
        </a>
    </div>
</div>

<div id="main">
    <input id="search" name="search" type="text" autocomplete="off" autofocus maxlength="6" placeholder="#Kurskod">

    <p id="errors">Inga fel></p>


    <div style="text-align: center;">
        <div id="loading" class="loading">
            <div id="escapingBall_1"></div>
        </div>
    </div>

    <div class="course-cont">
        <p id="recent">~Recent~</p>

        <div id="course-one" class="course button disabled">
            <h1>-</h1>
        </div>
        <div id="course-two" class="course button disabled">
            <h1>-</h1>
        </div>
    </div>
</div>

<div class="easter" id="simon">
    <img src="/assets/simon.jpg">

    <p>Jag heter Simon Bengtsson och jag är Awesome.</p>
</div>

<div class="easter" id="alle">
    <a href="https://www.facebook.com/alehar9320"><img
            src="/assets/alex.jpg"></a>

    <p>Jag heter Alexander Häremstam och jag är också Awesome. THE REAL ALEX THIS TIME!!! :))</p>
</div>
@stop
