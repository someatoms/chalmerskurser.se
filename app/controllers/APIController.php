<?php

/**
 * The Chalmers courses API currently has the following features:
 *
 * 1. Get course from course code
 *
 * The Course webpage API currently has the following features:
 *
 * 1. Create new notes
 * 2. Remove notes
 * 3. Get all notes
 * 4. Get the number of news items on the official course page
 * 5. Subscribe email for news updates
 *
 */
class APIController extends BaseController
{

    private $error = null;
    private $result = null;

    public function handleAPIRequest($list, $item = null)
    {
        switch ($list) {
            case "courses":
                $this->handleCourses($item);
                break;
            case "notes":
                $this->handleNotes($item);
                break;
            case "news":
                $this->result = Util::countNews();
                break;
            case "subscribers":
                $this->handleSubscribers($item);
                break;
            default:
                $this->error = "No action specified";
        }

        if ($this->error === null && $this->result === null) {
            $this->error = "Unnamed error";
        }
        return json_encode(array('success' => !$this->error, 'error' => $this->error, 'results' => $this->result));
    }

    private function handleCourses($item)
    {
        if ($item != null) {
            $this->error = "Specific courses cannot be retrieved, use search instead.";
            return;
        }

        if (!isset($_GET['searchTerm'])) {
            $this->error = "Right now, search is the only supported action";
            return;
        }

        try {
            $this->result = CourseSearcher::search($_GET['searchTerm']);
        } catch (Exception $e) {
            $this->error = $e->getMessage();
        }
    }

    private function handleNotes($item)
    {
        Switch (Request::getMethod()) {
            case 'GET':
                if ($item === null) {
                    $this->result = Util::getSavedNotes();
                } else {
                    $this->error = "Can't get a single note right now";
                }
                break;
            case 'POST':
                if ($item === null) {
                    if (!empty($_POST["title"]) && !empty($_POST["text"])) {
                        $note = new Note(Util::clean($_POST["title"]), Util::clean($_POST['text']));
                        Util::saveNote($note);
                        $this->result = $note->getHtml(true);
                    } else {
                        $this->error = "Missing title or text argument.";
                    }
                } else {
                    $this->error = "Can't make put request to a single note";
                }
                break;
            case 'DELETE':
                if ($item === null) {
                    $this->error = "You have to specify which note to delete";
                } else {
                    if (Util::removeNote($item)) {
                        $this->result = $item;
                    } else {
                        $this->error = "Couldn't remove note: " . $_POST['noteId'];
                    }
                }
                break;
            default:
                $this->error = "Notes method not supported: " . Request::getMethod();
                break;
        }

    }

    private function handleSubscribers($email)
    {
        Switch (Request::getMethod()) {
            case 'POST':
                if ($this->result = Util::addNewsWatcher($email)) {
                    $this->result = $email;
                } else {
                    $this->error = "Couldn't subscribe";
                }
                break;
            case 'GET':
                $this->result = Util::checkNews();
                break;
            default:
                $this->error = "Notes method not supported";
                break;
        }
    }

    public function git($command)
    {
        if ($command === 'pull') {
        	dd('not supported');
        }
    }

}