<?php

/**
 * Handling course route
 */
class CourseController extends BaseController
{
    public function showCourse($code)
    {
        if ($code === "tda416") {
            $course = new Course($code);
            if (!empty($_GET['page'])) {
                return View::make('course.basic', array('title' => 'TDA416', 'course' => $course, 'html' => CourseView::getPageHTML($_GET['page'])));
            } elseif(isset($_GET['unsubscribe'])) {
                if(Util::removeNewsWatcher($_GET['unsubscribe'])) {
                    $html = "<h4>No more news updates will be sent to: " . htmlentities($_GET['unsubscribe']) . "</h4>";
                } else {
                    $html = "<h4>This email is not subscribed for news items: " . htmlentities($_GET['unsubscribe']) . "</h4>";
                }
                return View::make('course.basic', array('html' => $html, 'course' => $course, 'title' => 'TDA416'));
            } else {
                return View::make('course.home', array('course' => $course, 'title' => 'TDA416'));
            }
        } else {
            try {
                $courses = CourseSearcher::search($code);
                if(isset($courses[0]->webpage)) {
                    return Redirect::away($courses[0]->webpage);
                } else {
                    $html = "<h4>Ingen kurshemsida för den här kursen kunde hittas.</h4>";
                    return View::make('course.basic', array('course' => $courses[0], 'html' => $html, 'title' => 'TDA416'));
                }
            } catch(Exception $e) {
                App::abort(404);
            }
        }
        return "404";
    }
}